pub mod linear_math;
pub mod dynamics;

use linear_math::jvec::JVec;
use linear_math::jmat::JMat;
use linear_math::jquaternion::JQuaternion;
use dynamics::rigid_body::RigidBody;

pub struct World {
    pub bodies: Vec<RigidBody>,
    pub gravity: JVec,
}

impl World {
    pub fn step(&mut self, timestep: f32) {
        self.integrate_forces(timestep);
        self.integrate(timestep);
    }

    fn integrate_forces(&mut self, timestep: f32) {
        for mut body in &mut self.bodies {
            if !body.is_static {
                // linear velocity
                body.linear_vel += (body.force * (body.inv_mass * timestep)) + (self.gravity * timestep);
                // angular velocity
                body.angular_vel += (body.torque * timestep).transform(&body.inv_inertia_world);
            }
            // zero forces
            body.force = JVec::zero();
            body.torque = JVec::zero();
        }
    }

    fn integrate(&mut self, timestep: f32) {
        for mut body in &mut self.bodies {
            if !body.is_static {
                // position
                body.position += body.linear_vel * timestep;
                // angular
                let axis: JVec;
                let angle = body.angular_vel.len();

                if angle < 0.001 {
                    axis = body.angular_vel * (0.5 * timestep - (timestep * timestep * timestep) * 0.020833333333 * angle * angle);
                } else {
                    axis = body.angular_vel * ((0.5 * angle * timestep).sin() / angle);
                }

                body.orientation = JMat::from((JQuaternion::new(axis.x, axis.y, axis.z, (angle * timestep * 0.5).cos()) 
                    * JQuaternion::from(body.orientation)).normalize());

                // todo - dampening 

                body.update();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

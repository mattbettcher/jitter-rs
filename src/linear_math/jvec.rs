use std::f32;
use std::ops::{Mul, Sub, Add, MulAssign, SubAssign, Neg, Rem, AddAssign};
use std::fmt::{Display, Formatter, Result};
use linear_math::jmat::JMat;

#[derive(Copy, Clone, Debug)]
pub struct JVec {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl JVec {
    const EPSILON_SQ: f32 = f32::EPSILON * f32::EPSILON;

    pub fn new(x: f32, y: f32, z: f32) -> Self {
        JVec { x, y, z }
    }

    pub fn zero() -> Self {
        JVec {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn left() -> Self {
        JVec {
            x: 1.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn right() -> Self {
        JVec {
            x: -1.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn up() -> Self {
        JVec {
            x: 0.0,
            y: 1.0,
            z: 0.0,
        }
    }

    pub fn down() -> Self {
        JVec {
            x: 0.0,
            y: -1.0,
            z: 0.0,
        }
    }

    pub fn backward() -> Self {
        JVec {
            x: 0.0,
            y: 0.0,
            z: 1.0,
        }
    }

    pub fn forward() -> Self {
        JVec {
            x: 0.0,
            y: 0.0,
            z: -1.0,
        }
    }

    pub fn one() -> Self {
        JVec {
            x: 1.0,
            y: 1.0,
            z: 1.0,
        }
    }

    pub fn min(a: &JVec, b: &JVec) -> Self {
        JVec {
            x: if a.x < b.x { a.x } else { b.x },
            y: if a.y < b.y { a.y } else { b.y },
            z: if a.z < b.z { a.z } else { b.z },
        }
    }

    pub fn max(a: &JVec, b: &JVec) -> Self {
        JVec {
            x: if a.x > b.x { a.x } else { b.x },
            y: if a.y > b.y { a.y } else { b.y },
            z: if a.z > b.z { a.z } else { b.z },
        }
    }

    pub fn is_zero(&self) -> bool {
        self.len_sq() == 0.0
    }

    pub fn is_near_zero(&self) -> bool {
        self.len_sq() < JVec::EPSILON_SQ
    }

    pub fn len(&self) -> f32 {
        ((self.x * self.x) + (self.y * self.y) + (self.z * self.z)).sqrt()
    }

    pub fn len_sq(&self) -> f32 {
        (self.x * self.x) + (self.y * self.y) + (self.z * self.z)
    }

    pub fn dot(a: &JVec, b: &JVec) -> f32 {
        (a.x * b.x) + (a.y * b.y) + (a.z * b.z)
    }

    pub fn cross(a: &JVec, b: &JVec) -> JVec {
        JVec {
            x: (a.y * b.z) - (a.z * b.y),
            y: (a.z * b.x) - (a.x * b.z),
            z: (a.x * b.y) - (a.y * b.x),
        }
    }

    pub fn normalize(&mut self) {
        let len_sq = (self.x * self.x) + (self.y * self.y) + (self.z * self.z);
        let inv_len = 1.0 / len_sq.sqrt();
        self.x *= inv_len;
        self.y *= inv_len;
        self.z *= inv_len;
    }

    pub fn swap(a: &mut JVec, b: &mut JVec) {
        let x = a.x;
        let y = a.y;
        let z = a.z;
        a.x = b.x;
        a.y = b.y;
        a.z = b.z;
        b.x = x;
        b.y = y;
        b.z = z;
    }

    pub fn transform(&self, mat: &JMat) -> JVec {
        JVec {
            x: ((self.x * mat.m11) + (self.y * mat.m21)) + self.z * mat.m31,
            y: ((self.x * mat.m12) + (self.y * mat.m22)) + self.z * mat.m32,
            z: ((self.x * mat.m13) + (self.y * mat.m23)) + self.z * mat.m33,
        }
    }
}

impl Add for JVec {
    type Output = JVec;

    fn add(self, other: JVec) -> JVec {
        JVec {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl AddAssign for JVec {
    fn add_assign(&mut self, other: JVec) {
        *self = JVec {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        };
    }
}

impl Sub for JVec {
    type Output = JVec;

    fn sub(self, other: JVec) -> JVec {
        JVec {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl SubAssign for JVec {
    fn sub_assign(&mut self, other: JVec) {
        *self = JVec {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        };
    }
}

impl Rem for JVec {
    type Output = JVec;

    fn rem(self, other: JVec) -> JVec {
        JVec::cross(&self, &other)
    }
}

impl Mul<f32> for JVec {
    type Output = JVec;

    fn mul(self, other: f32) -> JVec {
        JVec {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        }
    }
}

impl Mul<JVec> for f32 {
    type Output = JVec;

    fn mul(self, other: JVec) -> JVec {
        JVec {
            x: self * other.x,
            y: self * other.y,
            z: self * other.z,
        }
    }
}

impl MulAssign<f32> for JVec {
    fn mul_assign(&mut self, other: f32) {
        *self = JVec {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        };
    }
}

impl Mul<JVec> for JVec {
    type Output = f32;

    fn mul(self, other: JVec) -> f32 {
        JVec::dot(&self, &other)
    }
}

impl Neg for JVec {
    type Output = JVec;

    fn neg(self) -> JVec {
        JVec {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl PartialEq for JVec {
    fn eq(&self, other: &JVec) -> bool {
        self.x == other.x && self.y == other.y && self.z == other.z
    }
}

impl Eq for JVec {}

impl Display for JVec {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "x={}, y={}, z={}", self.x, self.y, self.z)
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn jvec_add() {
        let mut a = JVec::new(10.5, 9.5, 8.5);
        let b = JVec::new(9.5, 10.5, 11.5);

        assert_eq!(a + b, JVec::new(20.0, 20.0, 20.0));
        a += b;
        assert_eq!(a, JVec::new(20.0, 20.0, 20.0));
    }

    #[test]
    fn jvec_sub() {
        let mut a = JVec::new(10.5, 9.5, 8.5);
        let b = JVec::new(9.5, 10.5, 11.5);

        assert_eq!(a - b, JVec::new(1.0, -1.0, -3.0));
        a -= b;
        assert_eq!(a, JVec::new(1.0, -1.0, -3.0));
    }

    #[test]
    fn jvec_normalize() {
        let mut a = JVec::new(10.5, 9.5, 8.5);
        a.normalize();
        assert_eq!(a, JVec::new(0.6357801, 0.5752296, 0.51467913));
    }

    #[test]
    fn jvec_negate() {
        let a = JVec::new(10.5, 9.5, 8.5);
        assert_eq!(-a, JVec::new(-10.5, -9.5, -8.5));
    }

    #[test]
    fn jvec_equality() {
        let a = JVec::new(10.5, 9.5, 8.5);
        let b = JVec::new(10.5, 9.5, 8.5);
        assert_eq!(a == b, true);
        assert_eq!(a != b, false);
    }

    #[test]
    fn jvec_swap() {
        let mut a = JVec::new(10.5, 9.5, 8.5);
        let mut b = JVec::new(8.5, 10.5, 9.5);
        JVec::swap(&mut a, &mut b);
        assert_eq!(b, JVec::new(10.5, 9.5, 8.5));
        assert_eq!(a, JVec::new(8.5, 10.5, 9.5));
    }

    #[test]
    fn jvec_mul() {
        let a = JVec::new(10.5, 9.5, 8.5);
        assert_eq!(a * 2.0, JVec::new(21.0, 19.0, 17.0));
        assert_eq!(2.0 * a, JVec::new(21.0, 19.0, 17.0));
    }

    #[test]
    fn jvec_dot() {
        let a = JVec::new(10.5, 9.5, 8.5);
        assert_eq!(JVec::dot(&a, &a), 272.75);
        assert_eq!(a * a, 272.75);
    }

    #[test]
    fn jvec_cross() {
        let a = JVec::new(10.5, 9.5, 8.5);
        let b = JVec::new(8.5, 10.5, 9.5);
        assert_eq!(JVec::cross(&a, &b), JVec::new(1.0, -27.5, 29.5));
        assert_eq!(a % b, JVec::new(1.0, -27.5, 29.5));
    }

    #[test]
    fn jvec_length() {
        let a = JVec::new(10.5, 9.5, 8.5);
        assert_eq!(a.len(), 16.515144);
        assert_eq!(a.len_sq(), 272.75);
    }
}

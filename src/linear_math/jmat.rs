use std::f32;
use std::ops::*;
use linear_math::jquaternion::JQuaternion;

#[derive(Copy, Clone, Debug)]
pub struct JMat {
    pub m11: f32,
    pub m12: f32,
    pub m13: f32,
    pub m21: f32,
    pub m22: f32,
    pub m23: f32,
    pub m31: f32,
    pub m32: f32,
    pub m33: f32
}

impl JMat {
    pub fn new(m11: f32, m12: f32, m13: f32, m21: f32, m22: f32, m23: f32, m31: f32, m32: f32, m33: f32) -> Self {
        JMat {
            m11, m12, m13,
            m21, m22, m23,
            m31, m32, m33
        }
    }

    pub fn transpose(&self) -> JMat {
        JMat {
            m11: self.m11,
            m12: self.m21,
            m13: self.m31,
            m21: self.m12,
            m22: self.m22,
            m23: self.m32,
            m31: self.m13,
            m32: self.m23,
            m33: self.m33,
        }
    }
}


impl Mul<JMat> for JMat {
    type Output = JMat;

    fn mul(self, other: JMat) -> JMat {
        JMat {
            m11: (self.m11 * other.m11) + (self.m12 * other.m21) + (self.m13 * other.m31),
            m12: (self.m11 * other.m12) + (self.m12 * other.m22) + (self.m13 * other.m32),
            m13: (self.m11 * other.m13) + (self.m12 * other.m23) + (self.m13 * other.m33),
            m21: (self.m21 * other.m11) + (self.m22 * other.m21) + (self.m23 * other.m31),
            m22: (self.m21 * other.m12) + (self.m22 * other.m22) + (self.m23 * other.m32),
            m23: (self.m21 * other.m13) + (self.m22 * other.m23) + (self.m23 * other.m33),
            m31: (self.m31 * other.m11) + (self.m32 * other.m21) + (self.m33 * other.m31),
            m32: (self.m31 * other.m12) + (self.m32 * other.m22) + (self.m33 * other.m32),
            m33: (self.m31 * other.m13) + (self.m32 * other.m23) + (self.m33 * other.m33),
        }
    }
}

impl PartialEq for JMat {
    fn eq(&self, other: &JMat) -> bool {
        self.m11 == other.m11 && self.m12 == other.m12 && self.m13 == other.m13 &&
        self.m21 == other.m21 && self.m22 == other.m22 && self.m23 == other.m23 &&
        self.m31 == other.m31 && self.m32 == other.m32 && self.m33 == other.m33
    }
}

impl Eq for JMat {}

impl From<JQuaternion> for JMat {
    fn from(q: JQuaternion) -> Self {
        JMat {
            m11: 1.0 - (2.0 * ((q.y * q.y) + (q.z * q.z))),
            m12: 2.0 * ((q.x * q.y) + (q.z * q.w)),
            m13: 2.0 * ((q.z * q.x) - (q.y * q.w)),
            m21: 2.0 * ((q.x * q.y) - (q.z * q.w)),
            m22: 1.0 - (2.0 * ((q.z * q.z) + (q.x * q.x))),
            m23: 2.0 * ((q.y * q.z) + (q.x * q.w)),
            m31: 2.0 * ((q.z * q.w) + (q.y * q.w)),
            m32: 2.0 * ((q.y * q.z) - (q.x * q.w)),
            m33: 1.0 - (2.0 * ((q.y * q.y) + (q.x * q.x))),
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn jmat_mul() {
        let a = JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
        let b = JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);

        assert_eq!(a * b, JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0));
    }
}
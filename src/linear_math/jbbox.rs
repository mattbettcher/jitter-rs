use std::f32;
use std::ops::{Add, AddAssign, Mul, MulAssign, Neg, Rem, Sub, SubAssign};
use std::fmt::{Display, Formatter, Result};
use linear_math::jvec::JVec;

#[derive(Copy, Clone, Debug)]
pub struct JBBox {
    pub min: JVec,
    pub max: JVec,
}

pub enum Containment {
    Disjoint,
    Contained,
    Intersecting,
}

impl JBBox {
    pub fn new(min: JVec, max: JVec) -> Self {
        JBBox { min, max }
    }

    pub fn contains_point(&self, point: JVec) -> Containment {
        if self.min.x <= point.x && point.x <= self.max.x && self.min.y <= point.y
            && point.y <= self.max.y && self.min.z <= point.z && point.z <= self.max.z
        {
            Containment::Contained
        } else {
            Containment::Disjoint
        }
    }

    pub fn contains_bbox(&self, other: &Self) -> Containment {
        if self.max.x >= other.min.x && self.min.x <= self.max.x && self.max.y >= other.min.y
            && self.min.y <= self.max.y && self.max.z >= other.min.z
            && self.min.z <= self.max.z
        {
            if self.min.x <= other.min.x && other.max.x <= self.max.x && self.min.y <= other.min.y
                && other.max.y <= self.max.y && self.min.z <= other.min.z
                && other.max.z <= self.max.z
            {
                Containment::Contained
            } else {
                Containment::Intersecting
            }
        } else {
            Containment::Disjoint
        }
    }
}

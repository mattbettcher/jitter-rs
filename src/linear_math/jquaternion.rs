use std::f32;
use std::ops::{Mul};
use linear_math::jmat::JMat;

#[derive(Copy, Clone, Debug)]
pub struct JQuaternion {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32
}

impl JQuaternion {
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Self {
        JQuaternion { x, y, z, w }
    }

    pub fn normalize(&mut self) -> Self {
        let inv_len = 1.0 / ((self.x * self.x) + (self.y * self.y) + (self.z * self.z) + (self.w * self.w)).sqrt();
        self.x *= inv_len;
        self.y *= inv_len;
        self.z *= inv_len;
        self.w *= inv_len;
        *self
    }
}

impl Mul<JQuaternion> for JQuaternion {
    type Output = JQuaternion;

    fn mul(self, other: JQuaternion) -> JQuaternion {
        JQuaternion {
            x: ((self.x * other.w) + (other.x * self.w)) + (self.y * other.z) - (self.z * other.y),
            y: ((self.y * other.w) + (other.y * self.w)) + (self.z * other.x) - (self.x * other.z),
            z: ((self.z * other.w) + (other.z * self.w)) + (self.x * other.y) - (self.y * other.x),
            w: (self.w * other.w) - ((self.x * other.x) + (self.y * other.y) + (self.z * other.z)),
        }
    }
}

impl From<JMat> for JQuaternion {
    fn from(mat: JMat) -> Self {
        let scale = mat.m11 + mat.m22 + mat.m33;
        if scale > 0.0 {
            let sqrt_scale = (scale + 1.0).sqrt();
            let inv_sqrt_scale = 0.5 / sqrt_scale;
            JQuaternion {
                x: (mat.m23 - mat.m32) * inv_sqrt_scale,
                y: (mat.m31 - mat.m13) * inv_sqrt_scale,
                z: (mat.m12 - mat.m21) * inv_sqrt_scale,
                w: scale * 0.5,
            }
        }
        else if (mat.m11 >= mat.m22) && (mat.m11 >= mat.m33) {
            let sqrt_scale = (((1.0 + mat.m11) - mat.m22) - mat.m33).sqrt();
            let inv_sqrt_scale = 0.5 / sqrt_scale;
            JQuaternion {
                x: scale * 0.5,
                y: (mat.m12 + mat.m21) * inv_sqrt_scale,
                z: (mat.m13 + mat.m31) * inv_sqrt_scale,
                w: (mat.m23 - mat.m32) * inv_sqrt_scale,
            }
        }
        else if mat.m22 > mat.m33 {
            let sqrt_scale = (((1.0 + mat.m22) - mat.m11) - mat.m33).sqrt();
            let inv_sqrt_scale = 0.5 / sqrt_scale;
            JQuaternion {
                x: (mat.m21 + mat.m12) * inv_sqrt_scale,
                y: scale * 0.5,
                z: (mat.m32 + mat.m23) * inv_sqrt_scale,
                w: (mat.m31 - mat.m13) * inv_sqrt_scale,
            }
        }
        else {
            let sqrt_scale = (((1.0 + mat.m33) - mat.m11) - mat.m22).sqrt();
            let inv_sqrt_scale = 0.5 / sqrt_scale;
            JQuaternion {
                x: (mat.m31 + mat.m13) * inv_sqrt_scale,
                y: (mat.m32 + mat.m23) * inv_sqrt_scale,
                z: scale * 0.5,
                w: (mat.m12 - mat.m21) * inv_sqrt_scale,
            }
        }
    }
}
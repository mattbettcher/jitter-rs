use linear_math::jmat::JMat;
use linear_math::jvec::JVec;


pub struct RigidBody {
    pub inertia: JMat,
    pub orientation: JMat,
    pub position: JVec,
    pub linear_vel: JVec,
    pub angular_vel: JVec,
    pub mass: f32,
    pub force: JVec,
    pub torque: JVec,
    pub inv_mass: f32,
    pub inv_inertia_world: JMat,
    pub inv_inertia: JMat,
    pub inv_orientation: JMat,
    pub is_static: bool,
}

impl RigidBody {
    pub fn new() -> Self {
        RigidBody {
            inertia: JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
            orientation: JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
            position: JVec::new(100.0, 10.0, 0.0),
            linear_vel: JVec::zero(),
            angular_vel: JVec::zero(),
            mass: 1.0,
            inv_mass: 1.0,
            force: JVec::zero(),
            torque: JVec::zero(),
            inv_inertia_world: JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
            inv_inertia: JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
            inv_orientation: JMat::new(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
            is_static: false,
        }
    }

    pub fn apply_impulse(&mut self, impluse: JVec, relative_position: Option<JVec>) {
        assert!(!self.is_static, "Can't apply an impulse to a static body.");

        // calculate linear velocity
        self.linear_vel += self.inv_mass * impluse;     // scale impulse by mass and add to linear velocity
        // calculate angular velocity if we provide a relative position
        if let Some(rel_pos) = relative_position {
            self.angular_vel += (rel_pos % impluse).transform(&self.inv_inertia_world);
        }
    }

    pub fn add_force(&mut self, force: JVec, position: Option<JVec>) {
        self.force += force;
        if let Some(pos) = position {
            self.torque += (pos - self.position) % force;
        }
    }

    pub fn add_torque(&mut self, torque: JVec) {
        self.torque += torque;
    }

    pub fn update(&mut self) {
        self.inv_orientation = self.orientation.transpose();
        // update bounding box

        if !self.is_static {
            self.inv_inertia_world = (self.inv_orientation * self.inv_inertia) * self.orientation;
        }
    }
}
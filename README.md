# jitter-rs

[![pipeline status](https://gitlab.com/mattbettcher/jitter-rs/badges/master/pipeline.svg)](https://gitlab.com/mattbettcher/jitter-rs/commits/master)

A port of the Jitter physics library to Rust!